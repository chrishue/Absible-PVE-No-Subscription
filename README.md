# Ansible PVE-No-Subscription

A role for Proxmox Server, to set up the no-subscription repository

https://pve.proxmox.com/wiki/Package_Repositories#sysadmin_no_subscription_repo

## Requirements

- Ansible has root access

## Install with ansible-galaxy

Add or append this to your requirements.yml file.
```yml
---
roles:
- src: git@codeberg.org:chrishue/Ansible-PVE-No-Subscription.git
  scm: git
  name: chrishue.PVE-No-Subscription
```

```bash
ansible-galaxy install -r requirements.yml
```
> add `--force` to update.

## Usage example

```yml
---
- hosts: pvenodes
  roles:
  - role: chrishue.PVE-No-Subscription
    dist: bullseye
    become: yes
```
